# Web client

A flask app serving map with markers to front-end

## Run

```bash
gunicorn --workers 2 wsgi:app
```