from json import dumps

from flask import Blueprint, render_template, request

from app.ApiClient import coordinates_to_xy, places_by_coordinates, places_by_xy

main_bp = Blueprint(
    'main_bp',
    __name__,
    template_folder='templates',
    static_folder='frontend'
)


@main_bp.route('/')
def dejup():
    """Main map page."""
    return render_template('main_map.jinja2')


@main_bp.route('/associate/places')
def serve_places_associate():
    args = request.args
    n = args.get('n', default=0, type=float)
    s = args.get('s', default=0, type=float)
    e = args.get('e', default=0, type=float)
    w = args.get('w', default=0, type=float)
    source = args.get('source', default='dejup_complemented', type=str)
    limit = args.get('limit', default=100, type=int)

    if not n and not s and not e and not w:
        return dumps({'success': False}), 400

    hook = '/associations/dejup/within_coords'
    params = {'n': n, 's': s, 'e': e, 'w': w, 'limit': limit}
    if source == 'dejup_native':
        x_max, y_max = coordinates_to_xy(e, n)
        x_min, y_min = coordinates_to_xy(w, s)
        params = {'x_min': x_min, 'x_max': x_max, 'y_min': y_min, 'y_max': y_max}
        return places_by_xy(params)
    elif source == 'dejup_complemented':
        hook = '/associations/dejup/within_coords'
    elif source == 'edenred_native':
        hook = '/edenred/places_coords'
    elif source == 'edenred_complemented':
        hook = '/associations/edenred/within_coords'

    return places_by_coordinates(params, hook)
