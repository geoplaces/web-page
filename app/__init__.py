from os import environ

from flask import Flask
from werkzeug.utils import import_string

from . import routes

app = Flask(__name__)


def app_init():
    """Init the app."""

    cfg = import_string('configmodule.ProductionConfig')()
    if 'SERVER_CONF' in environ:
        cfg = import_string(environ['SERVER_CONF'])()

    app.config.from_object(cfg)

    with app.app_context():
        app.register_blueprint(routes.main_bp)
        return app
