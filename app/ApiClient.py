from datetime import datetime, timezone
from json import dumps
from typing import Dict

import requests
from flask import current_app


def get_token(force: bool = False) -> str:
    if not force:
        token = current_app.config.get('API_TOKEN')
        exp = current_app.config.get('API_TOKEN_EXP')
        if token and exp is not None and exp > datetime.now(timezone.utc).timestamp():
            return token

    body = {
        'scope': 'read:associate read:edenred read:dejup',
        'username': current_app.config['API_CLIENT'],
        'password': current_app.config['API_PASSW']
    }
    resp = requests.post(f'{current_app.config["DB_API_URL"]}/{current_app.config["DB_API_VERSION"]}/auth/token', data=body)
    if resp.status_code != 200:
        return ""

    token = resp.json().get('access_token')
    if not token:
        return ""

    current_app.config['API_TOKEN'] = token
    current_app.config['API_TOKEN_EXP'] = resp.json().get('exp')
    return token


def places_by_coordinates(coordinates: Dict, hook: str) -> tuple[str, int] | Dict:
    """Serves places based on the coordinates limit."""
    resp = requests.get(f'{current_app.config["DB_API_URL"]}/{current_app.config["DB_API_VERSION"]}{hook}',
                        headers={'Content-Type': 'application/json',
                                 'Authorization': f'Bearer {get_token()}'},
                        params=coordinates)

    if resp.status_code != 200:
        return dumps({'success': False}), 422

    return resp.json()


x_grad = 1.3615356822108318e-05
x_intr = -5.832692470523333
y_grad = 8.965168156107985e-06
y_intr = 27.082942839497104


def coordinates_to_xy(lon: float, lat: float) -> tuple[int, int]:
    return int((lon - x_intr) / x_grad), int((lat - y_intr) / y_grad)


def xy_to_coordinates(x: int, y: int) -> tuple[float, float]:
    return x_grad * x + x_intr, y_grad * y + y_intr


def places_by_xy(params: Dict) -> tuple[str, int] | Dict:
    resp = requests.get(f'{current_app.config["DB_API_URL"]}/{current_app.config["DB_API_VERSION"]}/dejup/places_coords',
                        headers={'Content-Type': 'application/json)',
                                 'Authorization': f'Bearer {get_token()}'},
                        params=params)

    if resp.status_code == 401:
        token = get_token(True)
        resp = requests.get(f'{current_app.config["DB_API_URL"]}/{current_app.config["DB_API_VERSION"]}/dejup/places_coords',
                            headers={'Content-Type': 'application/json',
                                     'Authorization': f'Bearer {token}'},
                            params=params)

    if resp.status_code != 200:
        return dumps({'success': False}), 422

    result = {
        'total': resp.json()['total'],
        'limit': resp.json()['limit'],
        'data': []
    }

    for place in resp.json()['data']:
        lon, lat = xy_to_coordinates(place['x'], place['y'])
        result['data'].append({
            'name': place['name'],
            'city': place['city'],
            'cat': place['cat'],
            'address': place['address'],
            'link': '',
            'lon': lon,
            'lat': lat
        })

    return result
