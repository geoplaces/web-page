"""Configuration settings."""

from os import getenv as ge

from dotenv import load_dotenv

load_dotenv()


class Config:
    """Default config."""
    DEBUG = False
    TESTING = False

    DB_API_URL = ge('DB_API_URL')
    API_CLIENT = ge('API_CLIENT')
    API_PASSW = ge('API_PASSW')

    DB_API_VERSION = 'v1'


class ProductionConfig(Config):
    """Prod config."""
    PROD = True


class DevelopmentConfig(Config):
    """Dev config."""
    DEBUG = True


class TestingConfig(Config):
    """Testing (staging) config."""
    DEBUG = True
    TESTING = True
