FROM python:3.10.6-slim-buster

WORKDIR /usr/src/app

COPY . /usr/src/app/
COPY .env_example /usr/src/app/.env

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r requirements.txt

CMD gunicorn --workers 2 -b 0.0.0.0:"$PORT" wsgi:app
