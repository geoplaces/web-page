import pytest
import multiprocessing
from unittest.mock import patch

from werkzeug.utils import import_string

from app import app_init

SAMPLE_RESP = {
    "data":
    [
        {
            "lat": 48.7016511,
            "lon": 2.1354273,
            "name": "SPEED WICHES"
        }
    ],
    "limit": 100,
    "total": 1
}


@pytest.fixture(scope='session', autouse=True)
def app():
    """Initialize app + user record in db."""
    multiprocessing.set_start_method("fork")
    app = app_init()
    cfg = import_string('configmodule.TestingConfig')

    with app.app_context():
        yield app
