import pytest
from flask import url_for

@pytest.mark.usefixtures('live_server')
class TestEndpoints:
    def test_main_page(self, client):
        resp = client.get(url_for('main_bp.dejup'))
        assert resp.status_code == 200

    # # @patch('app.ApiClient.places_by_coordinates', return_value=SAMPLE_RESP)
    # def test_coordinates_endpoint(self, mocker, client):
    #     mocker.patch('app.ApiClient.places_by_coordinates', return_value=SAMPLE_RESP)
    #     params={'N': 0.1, 'S': 0.1, 'E': 0.1, 'W': 0.1}
    #     print(url_for('main_bp.serve_places_associate', **params))
    #     resp = client.get(url_for('main_bp.serve_places_associate', **params))
    #     assert resp.status_code == 200
    #     # api_call.assert_called()
    #     assert len(resp.json['data'])> 0

